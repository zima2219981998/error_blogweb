/**
 * @Route("/add/friend/{id}", name="blog_add_friend")
 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
 * @param User $user
 */
public function addFriendAction(User $user, $id)
{
    $this->denyAccessUnlessGranted("ROLE_USER");

    $em = $this->getDoctrine()->getManager();
    $em->getRepository('ZimaBlogwebBundle:User')->find($id);

    $user->setOwners($this->getUser());
    $user->setFriends($id);

    $em->persist($user);
    $em->flush();

    return $this->redirectToRoute("blog_tab_friends", ['username' => $this->getUser()]);
}


WIDOK:
    <a href="{{ path('blog_add_friend', {'id': info.id}) }}">Follow</a>

ERROR:
Type error: Argument 1 passed to Doctrine\Common\Collections\ArrayCollection::__construct() must be of the type array, string given, called in /Users/zima/projekty/blogweb/vendor/doctrine/orm/lib/Doctrine/ORM/UnitOfWork.php on line 605