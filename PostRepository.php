/**
     * @param User $user
     * @return array
     */
    public function selectFriendsPost(User $user)
    {
        return $this->createQueryBuilder("post")
            ->select('user')
            ->leftJoin('ZimaBlogwebBundle:User', 'user', 'WITH', 'user.friends = post.owner')
            ->where('post.owner = :userId')
            ->setParameter('userId', $user->getFriends())
            ->orderBy("post.createdAt", "DESC")
            ->getQuery()
            ->getResult();
    }